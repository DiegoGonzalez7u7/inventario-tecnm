export class proyectores{
  _id?:number;
  serie:string;
  marca:string;
  modelo:string;
  color:string;
  constructor(serie:string, marca:string, modelo:string, color:string){
    this.serie=serie;
    this.marca=marca;
    this.modelo=modelo;
    this.color=color;
  }
}
