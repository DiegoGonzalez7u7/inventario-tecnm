export class tablets{
  _id?:number;
  serie:string;
  imei:number;
  marca:string;
  ram:string;
  almacenamiento:string;
  constructor( serie:string,imei:number,marca:string,ram:string,almacenamiento:string){
    this.serie=serie;
    this.imei=imei;
    this.marca=marca;
    this.ram=ram;
    this.almacenamiento=almacenamiento;
  }
}
