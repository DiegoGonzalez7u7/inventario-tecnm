export class escritorios {
    _id?: number;
    serie: string;
    altura: string;
    anchura: string;
    color: string;
    constructor(serie: string, altura: string, anchura: string, color: string,) {
        this.serie = serie;
        this.altura=altura;
        this.anchura=anchura;
        this.color=color;
    }
}