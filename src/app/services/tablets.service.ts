import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tablets } from '../models/tablets';
@Injectable({
  providedIn: 'root'
})
export class TabletsService {
  url='http://localhost:4000/api/tablets/'
  constructor(private http: HttpClient) { }

  getTablets(): Observable<any>{
    return this.http.get(this.url);
  }

  eliminarTablets(id:string): Observable<any>{
    return this.http.delete(this.url+id);
  }

  guardarTablets(tablets: tablets): Observable<any>{
    return this.http.post(this.url, tablets);
  }
  editarTablets(id: string, tablets: tablets): Observable<any>{
    return this.http.put(this.url + id, tablets);
  }
  obtenerTablets(id: string): Observable<any>{
    return this.http.get(this.url + id);
  }
}
