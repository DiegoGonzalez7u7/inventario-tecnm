import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { escritorios } from '../models/escritorios';
@Injectable({
  providedIn: 'root'
})
export class EscritoriosService {
  url='http://localhost:4000/api/escritorios/'
  constructor(private http: HttpClient) { }

  getEscritorios(): Observable<any>{
    return this.http.get(this.url);
  }

  eliminarEscritorios(id:string): Observable<any>{
    return this.http.delete(this.url+id);
  }

  guardarEscritorios(escritorios: escritorios): Observable<any>{
    return this.http.post(this.url, escritorios);
  }
  editarEscritorios(id: string, escritorios: escritorios): Observable<any>{
    return this.http.put(this.url + id, escritorios);
  }
  obtenerEscritorios(id: string): Observable<any>{
    return this.http.get(this.url + id);
  }
}
