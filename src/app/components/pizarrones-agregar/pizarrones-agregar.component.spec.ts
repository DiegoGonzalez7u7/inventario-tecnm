import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PizarronesAgregarComponent } from './pizarrones-agregar.component';

describe('PizarronesAgregarComponent', () => {
  let component: PizarronesAgregarComponent;
  let fixture: ComponentFixture<PizarronesAgregarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PizarronesAgregarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PizarronesAgregarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
