import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabletsActualizarComponent } from './tablets-actualizar.component';

describe('TabletsActualizarComponent', () => {
  let component: TabletsActualizarComponent;
  let fixture: ComponentFixture<TabletsActualizarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabletsActualizarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TabletsActualizarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
