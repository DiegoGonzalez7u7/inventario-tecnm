import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SillasActualizarComponent } from './sillas-actualizar.component';

describe('SillasActualizarComponent', () => {
  let component: SillasActualizarComponent;
  let fixture: ComponentFixture<SillasActualizarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SillasActualizarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SillasActualizarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
