import { Component, OnInit } from '@angular/core';
import { proyectores } from 'src/app/models/proyectores';
import { ProyectoresService } from 'src/app/services/proyectores.service';
@Component({
  selector: 'app-proyectores-eliminar',
  templateUrl: './proyectores-eliminar.component.html',
  styleUrls: ['./proyectores-eliminar.component.css']
})
export class ProyectoresEliminarComponent implements OnInit {
  listProyectores: proyectores[] = [];
  constructor(private _proyectoresService: ProyectoresService){}
  ngOnInit(): void {
    this.obtenerProyectores();
  }
  obtenerProyectores() {
    this._proyectoresService.getProyectores().subscribe(data => {
      console.log(data);
      this.listProyectores = data;
    }, error => {
      console.log(error);
    })
  }
  eliminarProyectores(id:any){
    this._proyectoresService.eliminarProyectores(id).subscribe(data =>{
      console.log("eliminado");
      this.obtenerProyectores();
    })
  }
}
