import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabletsAgregarComponent } from './tablets-agregar.component';

describe('TabletsAgregarComponent', () => {
  let component: TabletsAgregarComponent;
  let fixture: ComponentFixture<TabletsAgregarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabletsAgregarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TabletsAgregarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
