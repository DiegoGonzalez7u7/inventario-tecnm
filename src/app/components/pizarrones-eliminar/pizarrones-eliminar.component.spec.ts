import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PizarronesEliminarComponent } from './pizarrones-eliminar.component';

describe('PizarronesEliminarComponent', () => {
  let component: PizarronesEliminarComponent;
  let fixture: ComponentFixture<PizarronesEliminarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PizarronesEliminarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PizarronesEliminarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
