import { Component, OnInit } from '@angular/core';
import { escritorios } from 'src/app/models/escritorios';
import { EscritoriosService } from 'src/app/services/escritorios.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: 'app-agregar-escritorios',
  templateUrl: './agregar-escritorios.component.html',
  styleUrls: ['./agregar-escritorios.component.css']
})
export class AgregarEscritoriosComponent implements OnInit {
  escritoriosForm: FormGroup;
  id: string | null;
  titulo = 'Insertar Escritorios';
  constructor(private fb: FormBuilder,
    private _escritoriosService: EscritoriosService,
    private aRouter: ActivatedRoute,) {

    this.escritoriosForm = this.fb.group({
      serie: ['', Validators.required],
      altura: ['', Validators.required],
      anchura: ['', Validators.required],
      color: ['', Validators.required],
    })

    this.id = this.aRouter.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.esEditar();
  }
  agregarEscritorio() {
    console.log(this.escritoriosForm);
    const ESCRITORIO: escritorios = {
      serie: this.escritoriosForm.get('serie')?.value,
      altura: this.escritoriosForm.get('altura')?.value,
      anchura: this.escritoriosForm.get('anchura')?.value,
      color: this.escritoriosForm.get('color')?.value,
    }

    if (this.id !== null) {
      //editamos producto
      this._escritoriosService.editarEscritorios(this.id, ESCRITORIO).subscribe(data => {
        console.log("actualizado");
      }, error => {
        console.log(error);
        this.escritoriosForm.reset();
      })
    }
    else {
      //agregamos producto
      console.log(ESCRITORIO);
      this._escritoriosService.guardarEscritorios(ESCRITORIO).subscribe(data => {
        console.log("Equipo Guardado");
      }, error => {
        console.log(error);
        this.escritoriosForm.reset();
      })
    }
  }
  esEditar() {
    if (this.id !== null) {
      this.titulo = 'Editar Escritorio';
      this._escritoriosService.obtenerEscritorios(this.id).subscribe(data => {
        this.escritoriosForm.setValue({
          serie: data.serie,
          altura: data.altura,
          anchura: data.anchura,
          color: data.color,
        })
      })
    }
  }
}
