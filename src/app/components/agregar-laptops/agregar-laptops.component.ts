import { Component, OnInit } from '@angular/core';
import { equipos } from 'src/app/models/equipos';
import { EquiposService } from 'src/app/services/equipos.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import{ActivatedRoute, Router} from '@angular/router'
@Component({
  selector: 'app-agregar-laptops',
  templateUrl: './agregar-laptops.component.html',
  styleUrls: ['./agregar-laptops.component.css']
})
export class AgregarLaptopsComponent implements OnInit {
  equipoForm: FormGroup;
  id: string |null;
  titulo= 'Insertar Equipo';

  constructor(private fb: FormBuilder,
    private _equiposService: EquiposService,
    private aRouter: ActivatedRoute,
    private router: Router,) {

    this.equipoForm= this.fb.group({
      serie:['',Validators.required],
      marca:['',Validators.required],
      procesador:['',Validators.required],
      ram:['',Validators.required],
      almacenamiento:['',Validators.required],
    })
    this.id= this.aRouter.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.esEditar();
  }

  agregarEquipo() {
    console.log(this.equipoForm);
    const EQUIPO: equipos={
      serie: this.equipoForm.get('serie')?.value,
      marca: this.equipoForm.get('marca')?.value,
      procesador: this.equipoForm.get('procesador')?.value,
      ram: this.equipoForm.get('ram')?.value,
      almacenamiento: this.equipoForm.get('almacenamiento')?.value,
    }

    if(this.id !==null){
      //editamos producto
      this._equiposService.editarEquipo(this.id,EQUIPO).subscribe(data =>{
        console.log("actualizado");
      }, error =>{
        console.log(error);
        this.equipoForm.reset();
      })
    }
    else{
      //agregamos producto
      console.log(EQUIPO);
      this._equiposService.guardarEquipo(EQUIPO).subscribe(data =>{
        console.log("Equipo Guardado");
      }, error =>{
        console.log(error);
        this.equipoForm.reset();
      })
    }
  }
  esEditar(){
    if(this.id !== null){
      this.titulo= 'Editar Equipo';
      this._equiposService.obtenerEquipo(this.id).subscribe(data =>{
        this.equipoForm.setValue({
          serie: data.serie,
          marca:data.marca,
          procesador:data.procesador,
          ram:data.ram,
          almacenamiento:data.almacenamiento,
        })
      })
    }
  }
}
