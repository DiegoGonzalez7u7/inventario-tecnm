import { Component, OnInit } from '@angular/core';
import { escritorios } from 'src/app/models/escritorios';
import { EscritoriosService } from 'src/app/services/escritorios.service';
@Component({
  selector: 'app-eliminar-escritorios',
  templateUrl: './eliminar-escritorios.component.html',
  styleUrls: ['./eliminar-escritorios.component.css']
})
export class EliminarEscritoriosComponent implements OnInit {
  listEscritorios: escritorios[] = [];
  constructor(private _escritoriosService: EscritoriosService) { }
  ngOnInit(): void {
    this.obtenerEscritorios();
  }
  obtenerEscritorios() {
    this._escritoriosService.getEscritorios().subscribe(data => {
      console.log(data);
      this.listEscritorios = data;
    }, error => {
      console.log(error);
    })
  }
  eliminarEscritorios(id:any){
    this._escritoriosService.eliminarEscritorios(id).subscribe(data =>{
      console.log("eliminado");
      this.obtenerEscritorios();
    })
  }
}
