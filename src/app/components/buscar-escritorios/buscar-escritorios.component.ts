import { Component, OnInit } from '@angular/core';
import { escritorios } from 'src/app/models/escritorios';
import { EscritoriosService } from 'src/app/services/escritorios.service';

@Component({
  selector: 'app-buscar-escritorios',
  templateUrl: './buscar-escritorios.component.html',
  styleUrls: ['./buscar-escritorios.component.css']
})
export class BuscarEscritoriosComponent implements OnInit{
  listEscritorios: escritorios[]=[];
  constructor(private _escritoriosService: EscritoriosService){}
  ngOnInit(): void {
      this.obtenerEscritorios();
  }
  obtenerEscritorios(){
    this._escritoriosService.getEscritorios().subscribe(data =>{
      console.log(data);
      this.listEscritorios=data;
    },error =>{
      console.log(error);
    })
  }
}
