import { Component, OnInit } from '@angular/core';
import { equipos } from 'src/app/models/equipos';
import { EquiposService } from 'src/app/services/equipos.service';

@Component({
  selector: 'app-eliminar-laptops',
  templateUrl: './eliminar-laptops.component.html',
  styleUrls: ['./eliminar-laptops.component.css']
})
export class EliminarLaptopsComponent implements OnInit{
  listEquipos: equipos[]=[];
  constructor(private _equiposService: EquiposService){}

  ngOnInit(): void {
      this.obtenerEquipos();
  }

  obtenerEquipos(){
    this._equiposService.getEquipos().subscribe(data =>{
      console.log(data);
      this.listEquipos=data;
    },error =>{
      console.log(error);
    })
  }
  eliminarEquipos(id:any){
    this._equiposService.eliminarEquipos(id).subscribe(data =>{
      console.log("eliminado");
      this.obtenerEquipos();
    })
  }
}
