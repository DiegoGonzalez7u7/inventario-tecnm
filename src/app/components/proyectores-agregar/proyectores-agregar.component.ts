import { Component, OnInit } from '@angular/core';
import { proyectores } from 'src/app/models/proyectores';
import { ProyectoresService } from 'src/app/services/proyectores.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router'
@Component({
  selector: 'app-proyectores-agregar',
  templateUrl: './proyectores-agregar.component.html',
  styleUrls: ['./proyectores-agregar.component.css']
})
export class ProyectoresAgregarComponent implements OnInit {
  proyectorForm: FormGroup;
  id: string | null;
  titulo = 'Insertar Proyector';
  constructor(private fb: FormBuilder,
    private _proyectoresService: ProyectoresService,
    private aRouter: ActivatedRoute,) {
      this.proyectorForm = this.fb.group({
        serie: ['', Validators.required],
        marca: ['', Validators.required],
        modelo: ['', Validators.required],
        color: ['', Validators.required],
      })
      this.id = this.aRouter.snapshot.paramMap.get('id');
    }
  ngOnInit(): void {
    this.esEditar();
  }
  agregarProyector() {
    console.log(this.proyectorForm);
    const PROYECTOR: proyectores={
      serie: this.proyectorForm.get('serie')?.value,
      marca: this.proyectorForm.get('marca')?.value,
      modelo: this.proyectorForm.get('modelo')?.value,
      color: this.proyectorForm.get('color')?.value,
    }

    if(this.id !==null){
      //editamos producto
      this._proyectoresService.editarProyectores(this.id,PROYECTOR).subscribe(data =>{
        console.log("actualizado");
      }, error =>{
        console.log(error);
        this.proyectorForm.reset();
      })
    }
    else{
      //agregamos producto
      console.log(PROYECTOR);
      this._proyectoresService.guardarProyectores(PROYECTOR).subscribe(data =>{
        console.log("Equipo Guardado");
      }, error =>{
        console.log(error);
        this.proyectorForm.reset();
      })
    }
  }
  esEditar(){
    if(this.id !== null){
      this.titulo= 'Editar Proyector';
      this._proyectoresService.obtenerProyectores(this.id).subscribe(data =>{
        this.proyectorForm.setValue({
          serie: data.serie,
          marca:data.marca,
          modelo:data.modelo,
          color:data.color,
        })
      })
    }
  }
}
