import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuscarLaptopsComponent } from './buscar-laptops.component';

describe('BuscarLaptopsComponent', () => {
  let component: BuscarLaptopsComponent;
  let fixture: ComponentFixture<BuscarLaptopsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuscarLaptopsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BuscarLaptopsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
