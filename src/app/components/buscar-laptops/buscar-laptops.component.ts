import { Component, OnInit } from '@angular/core';
import { equipos } from 'src/app/models/equipos';
import { EquiposService } from 'src/app/services/equipos.service';

@Component({
  selector: 'app-buscar-laptops',
  templateUrl: './buscar-laptops.component.html',
  styleUrls: ['./buscar-laptops.component.css']
})
export class BuscarLaptopsComponent implements OnInit {
  listEquipos: equipos[]=[];

  constructor(private _equiposService: EquiposService){}

  ngOnInit(): void {
      this.obtenerEquipos();
  }

  obtenerEquipos(){
    this._equiposService.getEquipos().subscribe(data =>{
      console.log(data);
      this.listEquipos=data;
    },error =>{
      console.log(error);
    })
  }
}
