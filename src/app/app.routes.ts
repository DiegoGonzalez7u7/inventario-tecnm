import { Routes } from "@angular/router";
import { ActualizarEscritoriosComponent } from "./components/actualizar-escritorios/actualizar-escritorios.component";
import { ActualizarLaptopsComponent } from "./components/actualizar-laptops/actualizar-laptops.component";
import { AgregarEscritoriosComponent } from "./components/agregar-escritorios/agregar-escritorios.component";
import { AgregarLaptopsComponent } from "./components/agregar-laptops/agregar-laptops.component";
import { BuscarEscritoriosComponent } from "./components/buscar-escritorios/buscar-escritorios.component";
import { BuscarLaptopsComponent } from "./components/buscar-laptops/buscar-laptops.component";
import { EliminarEscritoriosComponent } from "./components/eliminar-escritorios/eliminar-escritorios.component";
import { EliminarLaptopsComponent } from "./components/eliminar-laptops/eliminar-laptops.component";
import { EscritoriosComponent } from "./components/escritorios/escritorios.component";
import { LaptopsComponent } from "./components/laptops/laptops.component";
import { LoginComponent } from "./components/login/login.component";
import { MenuComponent } from "./components/menu/menu.component";
import { PizarronesActualizarComponent } from "./components/pizarrones-actualizar/pizarrones-actualizar.component";
import { PizarronesAgregarComponent } from "./components/pizarrones-agregar/pizarrones-agregar.component";
import { PizarronesBuscarComponent } from "./components/pizarrones-buscar/pizarrones-buscar.component";
import { PizarronesEliminarComponent } from "./components/pizarrones-eliminar/pizarrones-eliminar.component";
import { PizarronesComponent } from "./components/pizarrones/pizarrones.component";
import { ProyectoresActualizarComponent } from "./components/proyectores-actualizar/proyectores-actualizar.component";
import { ProyectoresAgregarComponent } from "./components/proyectores-agregar/proyectores-agregar.component";
import { ProyectoresBuscarComponent } from "./components/proyectores-buscar/proyectores-buscar.component";
import { ProyectoresEliminarComponent } from "./components/proyectores-eliminar/proyectores-eliminar.component";
import { ProyectoresComponent } from "./components/proyectores/proyectores.component";
import { RegistrarComponent } from "./components/registrar/registrar.component";
import { SillasActualizarComponent } from "./components/sillas-actualizar/sillas-actualizar.component";
import { SillasAgregarComponent } from "./components/sillas-agregar/sillas-agregar.component";
import { SillasBuscarComponent } from "./components/sillas-buscar/sillas-buscar.component";
import { SillasEliminarComponent } from "./components/sillas-eliminar/sillas-eliminar.component";
import { SillasComponent } from "./components/sillas/sillas.component";
import { TabletsActualizarComponent } from "./components/tablets-actualizar/tablets-actualizar.component";
import { TabletsAgregarComponent } from "./components/tablets-agregar/tablets-agregar.component";
import { TabletsBuscarComponent } from "./components/tablets-buscar/tablets-buscar.component";
import { TabletsEliminarComponent } from "./components/tablets-eliminar/tablets-eliminar.component";
import { TabletsComponent } from "./components/tablets/tablets.component";

export const ROUTES: Routes = [
  // ruteo para el inicio de la pagina
  { path: 'home', component: LoginComponent },
  { path: 'registrar', component: RegistrarComponent },
  { path: 'menu', component: MenuComponent },
  // termina ruteo inicial

  // ruteo para laptops
  { path: 'equipo', component: LaptopsComponent },
  { path: 'equipo/agregar', component: AgregarLaptopsComponent },
  { path: 'equipo/agregar/:id', component: AgregarLaptopsComponent },
  { path: 'equipo/buscar', component: BuscarLaptopsComponent },
  { path: 'equipo/eliminar', component: EliminarLaptopsComponent },
  { path: 'equipo/actualizar', component: ActualizarLaptopsComponent },
  // termina ruteo para laptops

  // ruteo para tablets
  { path: 'tablets', component: TabletsComponent },
  { path: 'tablets/agregar', component: TabletsAgregarComponent },
  { path: 'tablets/agregar/:id', component: TabletsAgregarComponent },
  { path: 'tablets/eliminar', component: TabletsEliminarComponent },
  { path: 'tablets/buscar', component: TabletsBuscarComponent },
  { path: 'tablets/actualizar', component: TabletsActualizarComponent },
  //termina ruteo para tablets

  // ruteo de proyectores
  { path: 'proyectores', component: ProyectoresComponent },
  { path: 'proyectores/agregar', component: ProyectoresAgregarComponent },
  { path: 'proyectores/agregar/:id', component: ProyectoresAgregarComponent },
  { path: 'proyectores/eliminar', component: ProyectoresEliminarComponent },
  { path: 'proyectores/buscar', component: ProyectoresBuscarComponent },
  { path: 'proyectores/actualizar', component: ProyectoresActualizarComponent },
  // termina ruteo de proyectores

  // ruteo para escritorios
  { path: 'escritorios', component: EscritoriosComponent },
  { path: 'escritorios/agregar', component: AgregarEscritoriosComponent },
  { path: 'escritorios/agregar/:id', component: AgregarEscritoriosComponent },
  { path: 'escritorios/buscar', component: BuscarEscritoriosComponent },
  { path: 'escritorios/eliminar', component: EliminarEscritoriosComponent },
  { path: 'escritorios/actualizar', component: ActualizarEscritoriosComponent },
  // termina ruteo para escritorios

  // inicia ruteo para sillas
  { path: 'sillas', component: SillasComponent },
  { path: 'sillas/agregar', component: SillasAgregarComponent },
  { path: 'sillas/buscar', component: SillasBuscarComponent },
  { path: 'sillas/eliminar', component: SillasEliminarComponent },
  { path: 'sillas/actualizar', component: SillasActualizarComponent },
  // termina ruteo para sillas

  // inicia ruteo de pizarrones
  { path: 'pizarrones', component: PizarronesComponent },
  { path: 'pizarrones/agregar', component: PizarronesAgregarComponent },
  { path: 'pizarrones/buscar', component: PizarronesBuscarComponent },
  { path: 'pizarrones/eliminar', component: PizarronesEliminarComponent },
  { path: 'pizarrones/actualizar', component: PizarronesActualizarComponent },
  // finaliza ruteo de pizarrones

  // ruteo para urls vacias
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: '**', pathMatch: 'full', redirectTo: 'home' },
]
